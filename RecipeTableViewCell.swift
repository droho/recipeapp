//
//  RecipeTableViewCell.swift
//  RecipeApp
//
//  Created by Damian Drohobycki on 06/06/2019.
//  Copyright © 2019 Damian Drohobycki. All rights reserved.
//

import UIKit

class RecipeTableViewCell: UITableViewCell {

    var recipe: Recipe? {
        didSet{
            reciperImage.image = UIImage(named: recipe!.image)
            recipeTitle.text = recipe?.name
            recipeAuthor.text = "Author: \(recipe!.author)"
            recipeDescription.text = recipe?.desc
        }
    }
    
    @IBOutlet weak var reciperImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var recipeDescription: UILabel!
    @IBOutlet weak var recipeAuthor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
