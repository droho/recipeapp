//
//  Recipe.swift
//  RecipeApp
//
//  Created by Damian Drohobycki on 06/06/2019.
//  Copyright © 2019 Damian Drohobycki. All rights reserved.
//

import Foundation

struct Recipe {
    let image: String
    let name: String
    let desc: String
    let ingredients: String
    let difficulty: String
    let price: String
    let time: String
    let author: String
}
