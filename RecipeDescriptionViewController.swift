//
//  RecipeDescriptionViewController.swift
//  RecipeApp
//
//  Created by Damian Drohobycki on 06/06/2019.
//  Copyright © 2019 Damian Drohobycki. All rights reserved.
//

import UIKit

class RecipeDescriptionViewController: UIViewController {

    var recipe: Recipe? 

    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var difficulty: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var recipeDescription: UILabel!
    @IBOutlet weak var recipeIngredients: UILabel!
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recipeImage.image = UIImage(named: recipe!.image)
        name.text = recipe?.name
        difficulty.text = recipe?.difficulty
        price.text = recipe?.price
        time.text = recipe?.time
        recipeDescription.text = recipe?.desc
        recipeIngredients.text = recipe?.ingredients
        title = recipe?.name
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
