//
//  ViewController.swift
//  RecipeApp
//
//  Created by Damian Drohobycki on 06/06/2019.
//  Copyright © 2019 Damian Drohobycki. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var recipes: [Recipe] = []
    var selectedRow: Int = 0
    
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareRecipes()
    }
    
    private func prepareRecipes() {
        let salmon = Recipe(
            image: "salmon",
            name: "Chili-glazed Salmon",
            desc:
            """
            Preheat oven to 400°F (200˚C).
            In a bowl, mix together the salmon, chili sauce, and the scallions.
            Place the fillets on a baking tray lined with parchment paper. Spoon any leftover sauce on top of the salmon.
            Bake for 12-15 minutes, until the salmon is cooked but still tender.
            Enjoy!
            """,
            ingredients:
            """
            4 oz  salmon, 3 fillets (115 g)
            ½ cup  chili sauce (130 g)
            ¼ cup  fresh scallions, chopped (25 g)
            """,
            difficulty: "Easy",
            price: "Cheap",
            time: "20m",
            author: "Alvin"
        )
        let broccoli = Recipe(
            image: "braccoli",
            name: "Cheesy Garlic Broccoli",
            desc:
            """
            Preheat oven to 375ºF (190ºC).
            Spread out broccoli in a medium rectangular casserole dish.
            Stir the garlic and cheddar cheese together in a separate bowl.
            Sprinkle the cheese evenly over broccoli and add salt and pepper to taste.
            Bake for 25 minutes.
            Enjoy!
            """,
            ingredients:
            """
            3 crowns broccoli
            1 ¾ cups  shredded cheddar cheese (175 g)
            3 cloves garlic, crushed
            salt, to taste
            pepper, to taste
            """,
            difficulty: "Easy",
            price: "Cheap",
            time: "20m",
            author: "Alvin"
        )
        
        let pizza = Recipe(
            image: "pizza",
            name: "Vegan Pizza",
            desc:
            """
Preheat the oven to 400°F (200°C). Line a baking sheet with parchment paper.
Heat the olive oil in a medium pot over medium heat. Once the oil begins to shimmer, add the yellow onion and cook for 4-5 minutes, until semi-translucent. Add the garlic and cook for another 2-3 minutes, until fragrant. Add the jackfruit, salt, pepper, chili powder, paprika, cumin, and liquid smoke and stir until the jackfruit is well coated in the spices. Add the vegetable broth, cover, and cook for 15 minutes, until the jackfruit is tender enough to be mashed.
Mash the jackfruit until mostly broken apart and stringy. Transfer to a baking sheet and spread in an even layer. Bake for 20 minutes, until lightly browned.
Use your hands to stretch the pizza dough into a circle about 14 inches (35 cm) in diameter and ½-inch (1 ¼ cm) thick. Lay on a sheet of parchment paper.
Remove jackfruit from oven and increase the oven temperature to 425°F (220°C).
Pour ½ cup (145 g) of vegan barbecue sauce over the jackfruit, then toss to coat.
Spread the remaining ¼ cup (72g) barbecue sauce over the pizza dough in an even layer. Top with the jackfruit and red onion.
Bake for 14-16 minutes, until the crust is lightly golden around the edges.
Remove the pizza from the oven. Drizzle with the vegan ranch and garnish with cilantro leaves. Cut into 8 slices and serve.
Enjoy!
""",
            ingredients:
            """
            1 tablespoon  olive oil
            ½ yellow onion, sliced
            3 cloves garlic, minced
            40 oz  jackfruit, 2 cans, drained (1 kg)
            1 teaspoon  kosher salt
            1 teaspoon  pepper
            1 teaspoon  chili powder
            1 teaspoon  paprika
            1 teaspoon  ground cumin
            1 teaspoon  liquid smoke
            1 cup  low sodium vegetable broth (240 mL)
            1 lb  pizza dough (455 g)
            ¾ cup  vegan barbecue sauce, divided (210 g)
            red onion, sliced, to taste
            vegan ranch dressing, to taste
            fresh cilantro leaves, to tast
            """,
            difficulty: "Medium",
            price: "Cheap",
            time: "50m",
            author: "John"
        )
        
        let basilIce = Recipe(
            image: "basilIce",
            name: "Blueberry Basil Ice Pops",
            desc:
            """
            Make the simple syrup: In a small saucepan, combine the sugar, basil, and water. Bring to a boil over medium-high heat. Once boiling, cook for 2-3 minutes, until the sugar has dissolved and the basil is fragrant. Remove from the heat.
            Strain the herb syrup through a fine-mesh sieve set over a blender. Add the blueberries and lemon juice. Blend on high speed until smooth.
            Divide the mixture between 6 ice pop molds. Freeze until solid, at least 6 hours.
            Enjoy!
            """,
            ingredients:
            """
            ¾ cup  sugar (150 g)
            ½ cup  fresh basil, loosely packed (20 g)
            ½ cup  water (120 g)
            16 oz  frozen blueberry (455 g)
            1 tablespoon  lemon juice
            """,
            difficulty: "Easy",
            price: "Cheap",
            time: "6h",
            author: "John"
        )
        
        let cake = Recipe(
            image: "cake",
            name: "Ice cream cake",
            desc:
            """
            Preheat the oven to 350˚F (180˚C). Grease 2 8-inch (20 cm) round cake pans with nonstick spray.
            In a large bowl, combine the melted ice cream, cake mix, and egg whites. Whisk until smooth.
            Divide the cake batter evenly between the prepared pans. Bake according to the package instructions, about 30 minutes, or until a toothpick inserted in the center of the cakes comes out clean. Let cool completely, about 30 minutes.
            Place 1 cake on a cake stand or platter. Spread an even layer of frosting over the cake. Sprinkle ⅓ cup (65 G) sprinkles over the frosting. Top with the other cake layer. Spread a thin layer of frosting over the top and sides of the cake.
            Chill the cake in the refrigerator for 15 minutes.
            Use the remaining frosting to coat the top and sides of the cake. Cover the top of the cake with the remaining ⅓ cup sprinkles.
                Slice and serve.
            Enjoy!
            """,
            ingredients:
            """
            nonstick cooking spray, for greasing
            2 ½ cups  strawberry ice cream, melted (375 g)
            1 box sprinkle cake mix
            4 large egg whites
            4 cups  strawberry frosting (460 g)
            ⅔ cup  rainbow sprinkles, divided (135 g)
            """,
            difficulty: "Hard",
            price: "Expensive",
            time: "2h",
            author: "John"
        )
        
        recipes.append(salmon)
        recipes.append(broccoli)
        recipes.append(pizza)
        recipes.append(basilIce)
        recipes.append(cake)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let nav = segue.destination as? UINavigationController, let controller = nav.viewControllers.first as? RecipeDescriptionViewController else { return }
        controller.recipe = recipes[selectedRow]
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipeCell", for: indexPath) as! RecipeTableViewCell
        cell.recipe = recipes[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        performSegue(withIdentifier: "receipt", sender: self)
    }
    
}

